class Animal:

    male_chromosomes = "XY"
    female_chromosomes = "XX"

    def legs(self) -> None:
        return print("You have legs")

    def brain(self) -> None:
        return print("You have working brain")


class Dog(Animal): pass


creature = Dog()
print(creature.male_chromosomes)
print(creature.female_chromosomes)
creature.legs()
creature.brain()
