class Tree: pass


def print_name(name) -> None:
    return print(name)


def sum_three_num(first_num, second_num, third_num) -> None:
    result = first_num + second_num + third_num
    return print(result)


def hello_world() -> None:
    return print("Hello World")


def substract(first_num, second_num) -> None:
    result = first_num - second_num
    return print(result)


def create_class_obj(cls) -> None:
    cls_object = cls()
    return print(cls_object)


print_name("Jack")
sum_three_num(1, 2, 3)
hello_world()
substract(6, 2)
create_class_obj(Tree)
